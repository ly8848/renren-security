package io.renren.claz.jmh;

import io.renren.AdminApplication;
import io.renren.common.utils.RedisUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.All) // 测试方法平均执行时间
@OutputTimeUnit(TimeUnit.MILLISECONDS) // 输出结果的时间粒度为微秒
@State(Scope.Benchmark)
public class JMHDEMO {
    @Param({"10000", "100000", "1000000"})
    private int length;

    private ConfigurableApplicationContext context;
    RedisUtils redisUtils;

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(JMHDEMO.class.getName()+".*")
                .warmupIterations(1).measurementIterations(20).measurementBatchSize(100).forks(2).build();
        new Runner(options).run();
    }

    /**
     * setup初始化容器的时候只执行一次
     */
    @Setup(Level.Trial)
    public void init(){
        context = SpringApplication.run(AdminApplication.class);

        redisUtils = context.getBean(RedisUtils.class);
    }

    @Benchmark
    public void testGetPojo()
    {
        SysUserEntity user = new SysUserEntity();
        user.setEmail("123456@qq.com");
        redisUtils.set("user", user);
        redisUtils.get("user", SysUserEntity.class);
//        System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class)));
    }

}
