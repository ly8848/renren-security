package io.renren.claz;

import io.renren.modules.sys.dao.TestDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Map;
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserMapperTest {
    @Autowired
     TestDao publicMapper;
    @Test
    public void getPublicData(){
        String sql = "select now()";
        List<Map<String, Object>> list = publicMapper.selectPublicItemList(sql);
    }
}