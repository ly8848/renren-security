/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.job.task;

import io.renren.common.utils.DateUtil;
import io.renren.modules.sys.entity.XykBillDateEntity;
import io.renren.modules.sys.service.XykBillDateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 更新账单日期
 *
 * testTask为spring bean的名称
 *
 * @author Mark sunlightcs@gmail.com
 */
@Slf4j
@Component("updateBillDate")
public class UpdateBillDateTask implements ITask {

	@Resource
	private XykBillDateService xykBillDateService;

	@Override
	public void run(String params){
		log.warn("UpdateBillDate定时任务正在执行，参数为：{}", params);
		List<XykBillDateEntity> xykBillDateEntityList =  xykBillDateService.list();
		for (XykBillDateEntity xykBillDateEntity: xykBillDateEntityList) {
			//检查账单日大于当前日期  说明已经出账单  将账单日期 和还款日期都向后推迟一个月
			if(xykBillDateEntity.getBillDate().before(new Date())){
				xykBillDateEntity.setBillDate(DateUtil.addMonth(xykBillDateEntity.getBillDate(),1));
				xykBillDateEntity.setPayDate(DateUtil.addMonth(xykBillDateEntity.getPayDate(),1));
				xykBillDateService.updateById(xykBillDateEntity);
			}
		}
	}
}
