package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.XykAssertHistDao;
import io.renren.modules.sys.entity.XykAssertHistEntity;
import io.renren.modules.sys.service.XykAssertHistService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("xykAssertHistService")
public class XykAssertHistServiceImpl extends ServiceImpl<XykAssertHistDao, XykAssertHistEntity> implements XykAssertHistService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<XykAssertHistEntity> page = this.page(
                new Query<XykAssertHistEntity>().getPage(params),
                new QueryWrapper<XykAssertHistEntity>().orderByDesc("create_date")
        );

        return new PageUtils(page);
    }

}
