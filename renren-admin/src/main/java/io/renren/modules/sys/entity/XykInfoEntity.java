package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-25 11:29:39
 */
@Data
@TableName("xyk_info")
public class XykInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号

	 */
	@TableId
	private Integer seqNo;
	/**
	 * 信用卡开户行
	 */
	private String cardBank;
	/**
	 * 账单日
	 */
	private String billDate;
	/**
	 * 还款日
	 */
	private String payDate;
	/**
	 * 插入时间
	 */
	private Date createDate;
	/**
	 * 更新时间
	 */
	private Date updateDate;
	/**
	 * 账单期限
	 */
	private String period;
	/**
	 * 备注
	 */
	private String remark;

}
