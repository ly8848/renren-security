package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.XykInfoDao;
import io.renren.modules.sys.entity.XykInfoEntity;
import io.renren.modules.sys.service.XykInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("xykInfoService")
public class XykInfoServiceImpl extends ServiceImpl<XykInfoDao, XykInfoEntity> implements XykInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<XykInfoEntity> page = this.page(
                new Query<XykInfoEntity>().getPage(params),
                new QueryWrapper<XykInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List listCardBank() {
        List<XykInfoEntity> xykInfoEntityList = baseMapper.listCardBank();
        return xykInfoEntityList;
    }


}
