package io.renren.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import io.renren.common.utils.DateUtil;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.XykBillDateEntity;
import io.renren.modules.sys.entity.XykTranHistEntity;
import io.renren.modules.sys.service.XykAssertService;
import io.renren.modules.sys.service.XykBillDateService;
import io.renren.modules.sys.service.XykTranHistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-24 16:56:37
 */
@RestController
@RequestMapping("sys/xyktranhist")
@Slf4j
@Api(tags="信用卡交易流水接口")
public class XykTranHistController {
    @Autowired
    private XykTranHistService xykTranHistService;
    @Autowired
    private XykBillDateService xykBillDateService;
    @Autowired
    private XykAssertService xykAssertService;

    /**
     * 列表
     */
    @ApiOperation(value = "列表",notes = "信用卡交易历史列表" ,httpMethod="POST")
    @RequestMapping("/list")
    @RequiresPermissions("sys:xyktranhist:list")
    public R list(@RequestParam Map<String, Object> params){

        PageUtils page = xykTranHistService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xyktranhist:info")
    @ApiOperation(value = "详细信息",notes = "详细信息" ,httpMethod="POST")
    public R info(@PathVariable("seqNo") @ApiParam(value = "序号", required = true) Integer seqNo){
        XykTranHistEntity xykTranHist = xykTranHistService.getById(seqNo);

        return R.ok().put("xykTranHist", xykTranHist);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存",notes = "保存" ,httpMethod="POST")
    @ApiImplicitParam(name = "xykTranHist", value = "交易流水实体xykTranHist", required = true, dataType = "XykTranHistEntity")
    @RequestMapping("/save")
    @RequiresPermissions("sys:xyktranhist:save")
    public R save(@RequestBody XykTranHistEntity xykTranHist){
        //如果不上送 自动默认0.38的利率计算
        if(null == xykTranHist.getInAmt() ){
            xykTranHist.setInAmt(new BigDecimal(xykTranHist.getPrevAmt().intValue()*0.9962));
        }
        xykTranHist.setFeeAmt(xykTranHist.getPrevAmt().subtract(xykTranHist.getInAmt()));
        xykTranHist.setFeeRate(xykTranHist.getFeeAmt().divide(xykTranHist.getPrevAmt(),6, BigDecimal.ROUND_HALF_EVEN));
        setUseRate(xykTranHist);
        xykTranHist.setCreateDate(new Date());
        xykTranHist.setUpdateDate(new Date());
        if(xykTranHist.getRemark()  == null){
            xykTranHist.setRemark(xykTranHist.getCardBank());
        }
        xykTranHist.setStatus("1");
        xykTranHistService.save(xykTranHist);
        xykTranHistService.calAllFee(xykTranHist.getSeqNo());


        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:xyktranhist:update")
    public R update(@RequestBody XykTranHistEntity xykTranHist){
        ValidatorUtils.validateEntity(xykTranHist);
        xykTranHist.setFeeAmt(xykTranHist.getPrevAmt().subtract(xykTranHist.getInAmt()));
        xykTranHist.setFeeRate(xykTranHist.getFeeAmt().divide(xykTranHist.getPrevAmt(),6, BigDecimal.ROUND_HALF_EVEN));
        setUseRate(xykTranHist);


        xykTranHist.setUpdateDate(new Date());
        if(xykTranHist.getRemark()  == null){
            xykTranHist.setRemark(xykTranHist.getCardBank());
        }
        xykTranHistService.updateById(xykTranHist);
        xykTranHistService.calAllFee(xykTranHist.getSeqNo());


        return R.ok();
    }

    public XykTranHistEntity setUseRate( XykTranHistEntity xykTranHist){
        //获取当前日期到还款日期之间的天数
        XykBillDateEntity xykBillDateEntity = new XykBillDateEntity();
        xykBillDateEntity.setCardBank(xykTranHist.getCardBank());
        XykBillDateEntity xykBillDateEntity1 = xykBillDateService.getOne(new Wrapper<XykBillDateEntity>() {
            @Override
            public XykBillDateEntity getEntity() {
                return xykBillDateEntity;
            }

            @Override
            public MergeSegments getExpression() {
                return null;
            }

            @Override
            public String getSqlSegment() {
                return null;
            }
        });
        int days = DateUtil.differentDays(new Date(),xykBillDateEntity1.getPayDate());
        // 年化利率=费用/本金/天数*365
        xykTranHist.setUseRate(xykTranHist.getFeeAmt().
                divide(xykTranHist.getInAmt(),6, BigDecimal.ROUND_HALF_EVEN)
                .multiply(new BigDecimal(365))
                .divide(new BigDecimal(days),6, BigDecimal.ROUND_HALF_EVEN)
        );
        return xykTranHist;
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xyktranhist:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykTranHistService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

}
