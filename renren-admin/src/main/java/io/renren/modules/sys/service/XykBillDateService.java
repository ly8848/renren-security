package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykBillDateEntity;

import java.util.Map;

/**
 * 信用卡账单表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
public interface XykBillDateService extends IService<XykBillDateEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

