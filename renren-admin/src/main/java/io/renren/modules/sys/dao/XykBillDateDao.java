package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.XykBillDateEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信用卡账单表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
@Mapper
public interface XykBillDateDao extends BaseMapper<XykBillDateEntity> {
	
}
