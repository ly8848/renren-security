package io.renren.modules.sys.controller;

import io.renren.common.utils.DateUtil;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.XykBillDateEntity;
import io.renren.modules.sys.entity.XykBillDateHistEntity;
import io.renren.modules.sys.service.XykBillDateHistService;
import io.renren.modules.sys.service.XykBillDateService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 信用卡账单表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
@RestController
@RequestMapping("sys/xykbilldate")
public class XykBillDateController {
    @Autowired
    private XykBillDateService xykBillDateService;

    @Autowired
    private XykBillDateHistService xykBillDateHistService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:xykbilldate:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = xykBillDateService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xykbilldate:info")
    public R info(@PathVariable("seqNo") Integer seqNo){
        XykBillDateEntity xykBillDate = xykBillDateService.getById(seqNo);

        return R.ok().put("xykBillDate", xykBillDate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:xykbilldate:save")
    public R save(@RequestBody XykBillDateEntity xykBillDate){
        xykBillDate.setCreateDate(new Date());
        xykBillDate.setUpdateDate(new Date());
        int days = DateUtil.differentDays(xykBillDate.getBillDate(),xykBillDate.getPayDate());
        xykBillDate.setPeriod(String.valueOf(days));
        xykBillDateService.save(xykBillDate);
        BeanCopier bc =  BeanCopier.create(XykBillDateEntity.class, XykBillDateHistEntity.class,
                false);
        XykBillDateHistEntity xykBillDateHistEntity = new XykBillDateHistEntity();
        bc.copy(xykBillDate, xykBillDateHistEntity, null);
        xykBillDateHistService.save(xykBillDateHistEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:xykbilldate:update")
    public R update(@RequestBody XykBillDateEntity xykBillDate){
        ValidatorUtils.validateEntity(xykBillDate);
        xykBillDate.setUpdateDate(new Date());
        int days = DateUtil.differentDays(xykBillDate.getBillDate(),xykBillDate.getPayDate());
        xykBillDate.setPeriod(String.valueOf(days));
        xykBillDateService.updateById(xykBillDate);


        BeanCopier bc =  BeanCopier.create(XykBillDateEntity.class, XykBillDateHistEntity.class,
                false);
        XykBillDateHistEntity xykBillDateHistEntity = new XykBillDateHistEntity();
        bc.copy(xykBillDate, xykBillDateHistEntity, null);
        xykBillDateHistEntity.setCreateDate(new Date());
        xykBillDateHistService.save(xykBillDateHistEntity);

        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xykbilldate:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykBillDateService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

}
