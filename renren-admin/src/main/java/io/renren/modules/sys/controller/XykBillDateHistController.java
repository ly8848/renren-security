package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.entity.XykBillDateHistEntity;
import io.renren.modules.sys.service.XykBillDateHistService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 信用卡账单历史表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 09:01:22
 */
@RestController
@RequestMapping("sys/xykbilldatehist")
public class XykBillDateHistController {
    @Autowired
    private XykBillDateHistService xykBillDateHistService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:xykbilldatehist:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = xykBillDateHistService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xykbilldatehist:info")
    public R info(@PathVariable("seqNo") Integer seqNo){
        XykBillDateHistEntity xykBillDateHist = xykBillDateHistService.getById(seqNo);

        return R.ok().put("xykBillDateHist", xykBillDateHist);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:xykbilldatehist:save")
    public R save(@RequestBody XykBillDateHistEntity xykBillDateHist){
        xykBillDateHistService.save(xykBillDateHist);

        return R.ok();
    }

    /**
     * 修改
     */
//    @RequestMapping("/update")
//    @RequiresPermissions("sys:xykbilldatehist:update")
    public R update(@RequestBody XykBillDateHistEntity xykBillDateHist){
        ValidatorUtils.validateEntity(xykBillDateHist);
        xykBillDateHistService.updateById(xykBillDateHist);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xykbilldatehist:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykBillDateHistService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

}
