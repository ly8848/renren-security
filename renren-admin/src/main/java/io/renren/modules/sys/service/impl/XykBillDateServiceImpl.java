package io.renren.modules.sys.service.impl;

import io.renren.common.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.sys.dao.XykBillDateDao;
import io.renren.modules.sys.entity.XykBillDateEntity;
import io.renren.modules.sys.service.XykBillDateService;

@Slf4j
@Service("xykBillDateService")
public class XykBillDateServiceImpl extends ServiceImpl<XykBillDateDao, XykBillDateEntity> implements XykBillDateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<XykBillDateEntity> page = this.page(
                new Query<XykBillDateEntity>().getPage(params),
                new QueryWrapper<XykBillDateEntity>()
        );

        List<XykBillDateEntity> xykBillDateEntityList = new ArrayList<>() ;
        for (XykBillDateEntity x :page.getRecords()) {
            x.setPeriod(String.valueOf(DateUtil.differentDays(DateUtil.addMonth(x.getBillDate(),-1),x.getPayDate())));
            xykBillDateEntityList.add(x);
        }
        page.setRecords(xykBillDateEntityList);

        return new PageUtils(page);
    }

}
