package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.XykBillDateHistEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信用卡账单历史表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 09:01:22
 */
@Mapper
public interface XykBillDateHistDao extends BaseMapper<XykBillDateHistEntity> {
	
}
