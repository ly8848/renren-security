package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 信用卡账单表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
@Data
@TableName("xyk_bill_date")
public class XykBillDateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Integer seqNo;
	/**
	 * 信用卡开户行
	 */
	private String cardBank;
	/**
	 * 账单日
	 */
	@DateTimeFormat(pattern = "yyyyMMdd")
	@JsonFormat(locale="zh", pattern="yyyy-MM-dd")
	private Date billDate;
	/**
	 * 还款日
	 */
	@DateTimeFormat(pattern = "yyyyMMdd")
	@JsonFormat(locale="zh", pattern="yyyy-MM-dd")
	private Date payDate;
	/**
	 * 插入时间
	 */
	private Date createDate;
	/**
	 * 更新时间
	 */
	private Date updateDate;
	/**
	 * 账单期限
	 */
	private String period;
	/**
	 * 备注
	 */
	private String remark;

}
