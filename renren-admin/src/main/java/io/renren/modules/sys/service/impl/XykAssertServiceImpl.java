package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.XykAssertDao;
import io.renren.modules.sys.entity.XykAssertEntity;
import io.renren.modules.sys.service.XykAssertService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


/**
 * @author Administrator
 */
@Service("xykAssertService")
public class XykAssertServiceImpl extends ServiceImpl<XykAssertDao, XykAssertEntity> implements XykAssertService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        calTotalAmt();
        IPage<XykAssertEntity> page = this.page(
                new Query<XykAssertEntity>().getPage(params),
                new QueryWrapper<XykAssertEntity>().orderByAsc("lend_amt")

        );

        return new PageUtils(page);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void calTotalAmt() {
        baseMapper.calBankAmt();
        baseMapper.calTotalAmt();
    }

    @Override
    public boolean payback(String cardBank) {
        baseMapper.payback(cardBank);
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateBigAndSmall(){
        baseMapper.updateBigNum();
        baseMapper.updateSmallNum();
        baseMapper.updateTotalNum();
    }


}
