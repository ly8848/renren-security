package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykAssertHistEntity;

import java.util.Map;

/**
 * 信用卡资产还款历史表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-12-09 18:21:54
 */
public interface XykAssertHistService extends IService<XykAssertHistEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

