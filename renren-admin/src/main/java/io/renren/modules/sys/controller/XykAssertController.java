package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.XykAssertEntity;
import io.renren.modules.sys.entity.XykAssertHistEntity;
import io.renren.modules.sys.service.XykAssertHistService;
import io.renren.modules.sys.service.XykAssertService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 信用卡资产负债表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
@RestController
@RequestMapping("sys/xykassert")
public class XykAssertController {
    @Autowired
    private XykAssertService xykAssertService;
    @Autowired
    private XykAssertHistService xykAssertHistService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:xykassert:list")
    public R list(@RequestParam Map<String, Object> params){
        //更新大小额笔数及总数
        xykAssertService.updateBigAndSmall();

        PageUtils page = xykAssertService.queryPage(params);


        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xykassert:info")
    public R info(@PathVariable("seqNo") Integer seqNo){
        XykAssertEntity xykAssert = xykAssertService.getById(seqNo);

        return R.ok().put("xykAssert", xykAssert);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:xykassert:save")
    public R save(@RequestBody XykAssertEntity xykAssert){
        xykAssert.setCreateDate(new Date());
        xykAssert.setUpdateDate(new Date());
        if(xykAssert.getPayAmt() == null ){
            xykAssert.setPayAmt(new BigDecimal(0));
        }
        xykAssert.setRemark(xykAssert.getCardBank());
        xykAssertService.save(xykAssert);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:xykassert:update")
    public R update(@RequestBody XykAssertEntity xykAssert){
        ValidatorUtils.validateEntity(xykAssert);
        xykAssertService.updateById(xykAssert);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xykassert:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykAssertService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

    @RequestMapping("/payback")
    @RequiresPermissions("sys:xykassert:payback")
    public R payback(@RequestBody Integer[] seqNos){
        XykAssertEntity xykAssert = xykAssertService.getById(seqNos[0]);

        //保存到資產變更歷史表
        XykAssertHistEntity xykAssertHistEntity = new XykAssertHistEntity();
        xykAssertHistEntity.setCardBank(xykAssert.getCardBank());
        xykAssertHistEntity.setCreateDate(new Date());
        xykAssertHistEntity.setLendAmt(xykAssert.getLendAmt());
        xykAssertHistEntity.setPayAmt(xykAssert.getPayAmt());
        xykAssertHistEntity.setBigNum(xykAssert.getBigNum());
        xykAssertHistEntity.setSmallNum(xykAssert.getSmallNum());
        xykAssertHistEntity.setTotalNum(xykAssert.getTotalNum());
        xykAssertHistEntity.setPayDate(new Date());
        xykAssertHistEntity.setRemark("還款歷史記錄");
        xykAssertHistService.save(xykAssertHistEntity);
        //先保存在還款

        //将还款之后的信用卡大小额笔数清0
        xykAssert.setBigNum(0);
        xykAssert.setSmallNum(0);
        xykAssert.setTotalNum(0);
        xykAssertService.updateById(xykAssert);


        xykAssertService.payback(xykAssert.getCardBank());




        return R.ok();
    }

}
