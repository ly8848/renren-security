package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 信用卡资产还款历史表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-12-09 18:21:54
 */
@Data
@TableName("xyk_assert_hist")
public class XykAssertHistEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号

	 */
	@TableId
	private Integer seqNo;
	/**
	 * 信用卡开户行
	 */
	private String cardBank;
	/**
	 * 借款总金额
	 */
	private BigDecimal lendAmt;
	/**
	 * 手续费
	 */
	private BigDecimal payAmt;
	/**
	 * 还款时间
	 */
	private Date payDate;
	/**
	 * 插入时间
	 */
	private Date createDate;
	/**
	 * 备注
	 */
	private String remark;

	private Integer bigNum;
	private Integer smallNum;
	private Integer totalNum;

}
