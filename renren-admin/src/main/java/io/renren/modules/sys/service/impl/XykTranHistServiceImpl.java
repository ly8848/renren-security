package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.XykTranHistDao;
import io.renren.modules.sys.entity.XykTranHistEntity;
import io.renren.modules.sys.service.XykTranHistService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("xykTranHistService")
public class XykTranHistServiceImpl extends ServiceImpl<XykTranHistDao, XykTranHistEntity> implements XykTranHistService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String cardBank = (String)params.get("cardBank");
        String sort =(String)params.get("order");
        IPage<XykTranHistEntity> page = this.page(
                new Query<XykTranHistEntity>().getPage(params),
                new QueryWrapper<XykTranHistEntity>()
                        .like(StringUtils.isNotBlank(cardBank),"card_bank", cardBank)
                .orderByDesc("create_date")
                .eq("status","1")
        );

        return new PageUtils(page);
    }

    @Override
    public void calAllFee(Integer seqNo) {
        baseMapper.calAllFee(seqNo);
    }


}
