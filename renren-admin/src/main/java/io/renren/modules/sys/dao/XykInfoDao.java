package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.XykInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-25 11:29:39
 */
@Mapper
public interface XykInfoDao extends BaseMapper<XykInfoEntity> {
    List<XykInfoEntity> listCardBank();
}
