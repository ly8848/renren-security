package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-25 11:29:39
 */
public interface XykInfoService extends IService<XykInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取信用卡行名
     * @return
     */
    List<XykInfoEntity> listCardBank();
}

