package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.XykTranHistEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-24 16:56:37
 */
@Mapper
public interface XykTranHistDao extends BaseMapper<XykTranHistEntity> {
    /**
     * 计算最新一笔的时候最新的综合使用费率
     * @return
     */
    void calAllFee( Integer seqNo);
}
