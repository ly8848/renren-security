package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.XykAssertEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信用卡资产负债表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
@Mapper
public interface XykAssertDao extends BaseMapper<XykAssertEntity> {
    /**
     * 更新总额
     * @return
     */
    int calTotalAmt();

    /**
     * 更新各行余额
     * @return
     */
    int calBankAmt();



    /**
     * 还款
     * @param cardBank
     * @return
     */
    boolean payback( String cardBank);

    /**
     * 更新大小额笔数
     * @return
     */
    boolean updateBigNum();
    boolean updateSmallNum();
    boolean updateTotalNum();



}
