package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-24 16:56:37
 */
@Data
@TableName("xyk_tran_hist")
public class XykTranHistEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer seqNo;
	/**
	 * 信用卡开户行
	 */
	private String cardBank;
	/**
	 * 刷卡金额
	 */
	private BigDecimal prevAmt;
	/**
	 * 到账金额
	 */
	private BigDecimal inAmt;
	/**
	 * 刷卡手续费
	 */
	private BigDecimal feeAmt;
	/**
	 * 刷卡利率
	 */
	private BigDecimal feeRate;
	/**
	 * 资金使用利率
	 */
	private BigDecimal useRate;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 刷卡方式：微信，刷卡
	 */
	private String payType;
	/**
	 * 0 失效 1 生效	 */
	@JsonFormat
	private String status;

	/**
	 * 插入时间
	 */
	private Date createDate;
	/**
	 * 更新时间
	 */
	private Date updateDate;

}
