package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.XykAssertEntity;
import io.renren.modules.sys.entity.XykBillDateEntity;
import io.renren.modules.sys.entity.XykBillDateHistEntity;
import io.renren.modules.sys.entity.XykInfoEntity;
import io.renren.modules.sys.service.XykAssertService;
import io.renren.modules.sys.service.XykBillDateHistService;
import io.renren.modules.sys.service.XykBillDateService;
import io.renren.modules.sys.service.XykInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-25 11:29:39
 */
@RestController
@RequestMapping("sys/xykinfo")
public class XykInfoController {
    @Autowired
    private XykInfoService xykInfoService;
    @Autowired
    private XykBillDateService xykBillDateService;
    @Autowired
    private XykBillDateHistService xykBillDateHistService;
    @Autowired
    private XykAssertService xykAssertService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:xykinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = xykInfoService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 信用卡名称列表
     */
    @RequestMapping("/listCardBank")
//    @RequiresPermissions("sys:xykinfo:listCardName")
    public R listCardName(){
        List bankList = xykInfoService.listCardBank();

        return R.ok().put("page", bankList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xykinfo:info")
    public R info(@PathVariable("seqNo") Integer seqNo){
        XykInfoEntity xykInfo = xykInfoService.getById(seqNo);

        return R.ok().put("xykInfo", xykInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:xykinfo:save")
    @Transactional
    public R save(@RequestBody XykInfoEntity xykInfo){
        xykInfo.setUpdateDate(new Date());
        xykInfo.setCreateDate(new Date());
        xykInfo.setPeriod("48");
        xykInfoService.save(xykInfo);

        //保存信用卡资产信息
        XykAssertEntity xykAssertEntity =new XykAssertEntity();
        xykAssertEntity.setCreateDate(new Date());
        xykAssertEntity.setUpdateDate(new Date());
        xykAssertEntity.setCardBank(xykInfo.getCardBank());
        xykAssertEntity.setLendAmt(new BigDecimal(0));
        xykAssertEntity.setPayAmt(new BigDecimal(0));
        xykAssertEntity.setRemark(xykInfo.getCardBank());
        xykAssertService.save(xykAssertEntity);

        //保存信用卡账单信息
        XykBillDateEntity xykBillDate =new XykBillDateEntity();
        xykBillDate.setCreateDate(new Date());
        xykBillDate.setUpdateDate(new Date());
        xykBillDate.setCardBank(xykInfo.getCardBank());
        xykBillDate.setPeriod("0");
        xykBillDate.setBillDate(new Date());
        xykBillDate.setPayDate(new Date());
        xykBillDateService.save(xykBillDate);

        //保存信用卡账单历史信息
        BeanCopier bc =  BeanCopier.create(XykBillDateEntity.class, XykBillDateHistEntity.class,
                false);
        XykBillDateHistEntity xykBillDateHistEntity = new XykBillDateHistEntity();
        bc.copy(xykBillDate, xykBillDateHistEntity, null);
        xykBillDateHistService.save(xykBillDateHistEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:xykinfo:update")
    public R update(@RequestBody XykInfoEntity xykInfo){
        ValidatorUtils.validateEntity(xykInfo);
        xykInfo.setUpdateDate(new Date());
        xykInfoService.updateById(xykInfo);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xykinfo:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykInfoService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

}
