package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.XykAssertHistEntity;
import io.renren.modules.sys.service.XykAssertHistService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 信用卡资产还款历史表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-12-09 18:21:54
 */
@RestController
@RequestMapping("sys/xykasserthist")
public class XykAssertHistController {
    @Autowired
    private XykAssertHistService xykAssertHistService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:xykasserthist:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = xykAssertHistService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{seqNo}")
    @RequiresPermissions("sys:xykasserthist:info")
    public R info(@PathVariable("seqNo") Integer seqNo){
        XykAssertHistEntity xykAssertHist = xykAssertHistService.getById(seqNo);

        return R.ok().put("xykAssertHist", xykAssertHist);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:xykasserthist:save")
    public R save(@RequestBody XykAssertHistEntity xykAssertHist){
        xykAssertHistService.save(xykAssertHist);



        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:xykasserthist:update")
    public R update(@RequestBody XykAssertHistEntity xykAssertHist){
        ValidatorUtils.validateEntity(xykAssertHist);
        xykAssertHistService.updateById(xykAssertHist);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:xykasserthist:delete")
    public R delete(@RequestBody Integer[] seqNos){
        xykAssertHistService.removeByIds(Arrays.asList(seqNos));

        return R.ok();
    }

}
