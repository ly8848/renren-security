package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykAssertEntity;

import java.util.Map;

/**
 * 信用卡资产负债表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 08:17:15
 */
public interface XykAssertService extends IService<XykAssertEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void calTotalAmt();

    boolean payback( String cardBank);

    void updateBigAndSmall();

}

