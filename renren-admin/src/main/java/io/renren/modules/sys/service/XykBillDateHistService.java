package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykBillDateHistEntity;

import java.util.Map;

/**
 * 信用卡账单历史表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-26 09:01:22
 */
public interface XykBillDateHistService extends IService<XykBillDateHistEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

