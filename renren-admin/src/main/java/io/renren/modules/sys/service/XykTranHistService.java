package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.XykTranHistEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-11-24 16:56:37
 */
public interface XykTranHistService extends IService<XykTranHistEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void calAllFee(Integer seqNo);


}

