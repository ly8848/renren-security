package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.XykAssertHistEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信用卡资产还款历史表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-12-09 18:21:54
 */
@Mapper
public interface XykAssertHistDao extends BaseMapper<XykAssertHistEntity> {
	
}
