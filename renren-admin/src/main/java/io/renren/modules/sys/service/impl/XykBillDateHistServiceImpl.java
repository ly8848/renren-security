package io.renren.modules.sys.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.sys.dao.XykBillDateHistDao;
import io.renren.modules.sys.entity.XykBillDateHistEntity;
import io.renren.modules.sys.service.XykBillDateHistService;


@Service("xykBillDateHistService")
public class XykBillDateHistServiceImpl extends ServiceImpl<XykBillDateHistDao, XykBillDateHistEntity> implements XykBillDateHistService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<XykBillDateHistEntity> page = this.page(
                new Query<XykBillDateHistEntity>().getPage(params),
                new QueryWrapper<XykBillDateHistEntity>()
        );

        return new PageUtils(page);
    }

}
