package io.renren.learn.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "esb")
@EnableConfigurationProperties(MapConfig.class)
@RestController()
public class MapConfig {
    /**
     * 从配置文件中读取的limitSizeMap开头的数据
     * 注意：名称必须与配置文件中保持一致
     */
    private Map<String, String> infoMap = new HashMap<>();
    public Map<String, String> getInfoMap() {
        return infoMap;
    }
    public void setInfoMap(Map<String, String> infoMap) {
        this.infoMap = infoMap;
    }




}
