package io.renren.learn.inject;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 */
@Slf4j
@Component("taskA")
public class
TaskAHandler extends TaskAbstractHandler{
    @Override
    public boolean handleJob(String message) {
        System.out.println("Am taskA!");
        return false;
    }
}
