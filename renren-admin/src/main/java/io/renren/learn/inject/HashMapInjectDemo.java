package io.renren.learn.inject;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class HashMapInjectDemo {

    @Resource
    private Map<String, TaskAbstractHandler> taskHandlerMap;

    public void consume(String msgCode) {

        // 根据tag获取具体调用方
        TaskAbstractHandler taskHandler = taskHandlerMap.get(msgCode);

        taskHandler.handleJob("execute " + msgCode);
    }

}
