package io.renren.learn.inject;

/**
 * Map注入测试
 * @author Administrator
 */
public abstract class TaskAbstractHandler {
    abstract public boolean handleJob(String message);
}
