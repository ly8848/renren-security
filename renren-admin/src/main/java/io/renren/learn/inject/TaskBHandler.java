package io.renren.learn.inject;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 */
@Slf4j
@Component("taskB")
public class
TaskBHandler extends TaskAbstractHandler{
    @Override
    public boolean handleJob(String message) {
        System.out.println("Am taskB!");
        return false;
    }
}
