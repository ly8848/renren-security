package io.renren.learn.ibatis;

import java.util.List;
import java.util.Map;

public class ServiceRowsResult {
    private Boolean flag;
    private List<Map> page;

    public List<Map> getPage() {
        return page;
    }

    public void setPage(List<Map> page) {
        this.page = page;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public ServiceRowsResult(Boolean flag) {
        this.flag = flag;
    }
}
