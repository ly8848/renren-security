package io.renren.learn.ibatis;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/ap/generator/table")
public class TableController {
    @Resource
    TableDao tableDao;

//    @ApiDoc(name = "数据表列表", description = "")
    @RequestMapping("/list")
    public ServiceRowsResult list(String id) {
        ServiceRowsResult result = new ServiceRowsResult(true);
        result.setPage(tableDao.listTable());
        return result;
    }


//   @ApiDoc(name = "数据表结构", description = "")
    @RequestMapping("/columns")
    public ServiceRowsResult info(String tableName){
        ServiceRowsResult result = new ServiceRowsResult(true);
        result.setPage(tableDao.listTableColumn(tableName));
        return result;
    }
}