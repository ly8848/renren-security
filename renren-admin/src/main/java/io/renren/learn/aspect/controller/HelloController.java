package io.renren.learn.aspect.controller;

import io.renren.learn.config.MapConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author Administrator
 */
@Slf4j
@Controller("/aaos")
public class HelloController {

    @Resource
    MapConfig mapConfig;
    @GetMapping("/test/helloAop")
    public Object hello() {
        Map  map = mapConfig.getInfoMap();
        log.error((String) map.get("cbs0001"));
        log.error((String) map.get("cbs0002"));
        return map.get("cbs0001");
    }

    @GetMapping("/test/helloError")
    public Object helloError() {
        return 1 / 0;
    }

}
