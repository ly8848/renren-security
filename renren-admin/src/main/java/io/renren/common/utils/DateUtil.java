package io.renren.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    /**
     * date2比date1多的天数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int differentDays(Date beginDate, Date endDate)
    {
        int days = (int) ((endDate.getTime() - beginDate.getTime()) / (1000*3600*24));
        return days;
    }
    public static Date addMonth( Date date,int month){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH,month);
        return calendar.getTime();
    }

    public static void main(String[] args) {
        Date date = new Date();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        System.out.println(simpleDateFormat.format(date));
        System.out.println(simpleDateFormat.format(addMonth(date,2)));

    }
}
