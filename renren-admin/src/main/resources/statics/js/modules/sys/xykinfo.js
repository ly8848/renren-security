$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/xykinfo/list',
        datatype: "json",
        colModel: [			
			{ label: '序号', name: 'seqNo', index: 'seq_no', width: 50, key: true },
			{ label: '信用卡开户行', name: 'cardBank', index: 'card_bank', width: 80 }, 			
			{ label: '账单日', name: 'billDate', index: 'bill_date', width: 120 },
			{ label: '还款日', name: 'payDate', index: 'pay_date', width: 120 },
            { label: '账单期限', name: 'period', index: 'period', width: 80 },
            { label: '插入时间', name: 'createDate', index: 'create_date', width: 80 },
			{ label: '更新时间', name: 'updateDate', index: 'update_date', width: 80 }, 			
			{ label: '备注', name: 'remark', index: 'remark', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
            $("#jqGrid").hideCol("seqNo");
            $("#jqGrid").hideCol("period");
            $("#jqGrid").hideCol("createDate");
            $("#jqGrid").hideCol("updateDate");
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		xykInfo: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.xykInfo = {};
		},
		update: function (event) {
			var seqNo = getSelectedRow();
			if(seqNo == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(seqNo)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.xykInfo.seqNo == null ? "sys/xykinfo/save" : "sys/xykinfo/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.xykInfo),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var seqNos = getSelectedRows();
			if(seqNos == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "sys/xykinfo/delete",
                        contentType: "application/json",
                        data: JSON.stringify(seqNos),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(seqNo){
			$.get(baseURL + "sys/xykinfo/info/"+seqNo, function(r){
                vm.xykInfo = r.xykInfo;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});