$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/xyktranhist/list',
        datatype: "json",
        colModel: [			
			{ label: '序号', name: 'seqNo', index: 'seq_no', width: 50, key: true},
			{ label: '信用卡开户行', name: 'cardBank', index: 'card_bank', width: 80 }, 			
			{ label: '刷卡金额', name: 'prevAmt', index: 'prev_amt', width: 80 }, 			
			{ label: '到账金额', name: 'inAmt', index: 'in_amt', width: 60 },
			{ label: '刷卡手续费', name: 'feeAmt', index: 'fee_amt', width: 80 }, 			
			{ label: '刷卡利率', name: 'feeRate', index: 'fee_rate', width: 80 },
            { label: '资金使用利率', name: 'useRate', index: 'use_rate', width: 80 },
            { label: '生效标志', name: 'status', index: 'status', width: 60 },
            { label: '备注', name: 'remark', index: 'remark', width: 180 },
			{ label: '刷卡方式', name: 'payType', index: 'pay_type', width: 60 },
			{ label: '插入时间', name: 'createDate', index: 'create_date', width: 80 }, 			
			{ label: '更新时间', name: 'updateDate', index: 'update_date', width: 80 }			
        ],
		viewrecords: true,
        height: 600,
        rowNum: 16,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
           // $("#selector").find("option[value='微信']").prop("selected",true);
            $("#jqGrid").hideCol("seqNo");
            $("#jqGrid").hideCol("status");

        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
            cardBank: null
        },
        // couponSelected: '',
		showList: true,
		title: null,
        //信用卡开户行列表
        cardBankList:{},
		xykTranHist: {
            cardBank:'',
            payType:''
        }
	},
    created(){
	    //从数据库中取出所有的信用卡信息

        //如果没有这句代码，select中初始化会是空白的，默认选中就无法实现
        // this.couponSelected = this.cardBankList[0].cardBank;
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
            vm.showList = false;
			vm.title = "新增";
            vm.cardBankList={};
            $.get(baseURL + "sys/xykinfo/listCardBank/", function(r){
                vm.cardBankList = r.page;
            });
            vm.xykTranHist = {
                // cardBank:'浦发',
                payType:'云闪付'
            };
            console.log(vm.xykTranHist.payType);
            console.log(vm.xykTranHist.cardBank);


        },
		update: function (event) {
			var seqNo = getSelectedRow();
			if(seqNo == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(seqNo)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.xykTranHist.seqNo == null ? "sys/xyktranhist/save" : "sys/xyktranhist/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.xykTranHist),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var seqNos = getSelectedRows();
			if(seqNos == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "sys/xyktranhist/delete",
                        contentType: "application/json",
                        data: JSON.stringify(seqNos),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(seqNo){
			$.get(baseURL + "sys/xyktranhist/info/"+seqNo, function(r){
                vm.xykTranHist = r.xykTranHist;
                console.log(vm.xykTranHist.cardBank);
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{'cardBank': vm.q.cardBank},
                page:page
            }).trigger("reloadGrid");
		},
        getSelected(){
            //获取选中的数据
            // console.log(this.couponSelected)
        }

	}
});