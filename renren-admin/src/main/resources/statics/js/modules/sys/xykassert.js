$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/xykassert/list',
        datatype: "json",
        colModel: [			
			{ label: 'seqNo', name: 'seqNo', index: 'seq_no', width: 50, key: true },
			{ label: '信用卡开户行', name: 'cardBank', index: 'card_bank', width: 80 }, 			
			{ label: '借款总金额', name: 'lendAmt', index: 'lend_amt', width: 80 }, 			
			{ label: '手续费', name: 'payAmt', index: 'pay_amt', width: 80 },
            { label: '大额笔数', name: 'bigNum', index: 'big_num', width: 80 },
            { label: '小额笔数', name: 'smallNum', index: 'small_num', width: 80 },
            { label: '总笔数', name: 'totalNum', index: 'total_num', width: 80 },
			{ label: '插入时间', name: 'createDate', index: 'create_date', width: 80 }, 			
			{ label: '更新时间', name: 'updateDate', index: 'update_date', width: 80 }, 			
			{ label: '备注', name: 'remark', index: 'remark', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
            $("#jqGrid").hideCol("createDate");
            $("#jqGrid").hideCol("updateDate");
            $("#jqGrid").hideCol("seqNo");
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		xykAssert: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.xykAssert = {};
		},
		update: function (event) {
			var seqNo = getSelectedRow();
			if(seqNo == null){
				return ;
			}
			vm.showList = false;
            vm.title = "还款";
            
            vm.getInfo(seqNo)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.xykAssert.seqNo == null ? "sys/xykassert/save" : "sys/xykassert/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.xykAssert),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var seqNos = getSelectedRows();
			if(seqNos == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "sys/xykassert/delete",
                        contentType: "application/json",
                        data: JSON.stringify(seqNos),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
        payback: function (event) {
            var seqNos = getSelectedRows();
            if(seqNos == null){
                return ;
            }
            var lock = false;
            layer.confirm('确定要进行还款？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                if(!lock) {
                    lock = true;
                    $.ajax({
                        type: "POST",
                        url: baseURL + "sys/xykassert/payback",
                        contentType: "application/json",
                        data: JSON.stringify(seqNos),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("还款成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
                    });
                }
            }, function(){
            });
        },
		getInfo: function(seqNo){
			$.get(baseURL + "sys/xykassert/info/"+seqNo, function(r){
                vm.xykAssert = r.xykAssert;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});