/*
 Navicat Premium Data Transfer

 Source Server         : renren_security
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : renren_security

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 03/12/2021 17:35:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for xyk_assert
-- ----------------------------
DROP TABLE IF EXISTS `xyk_assert`;
CREATE TABLE `xyk_assert`  (
  `seq_no` int(0) NOT NULL AUTO_INCREMENT COMMENT '序号\r\n',
  `card_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信用卡开户行',
  `lend_amt` decimal(10, 2) UNSIGNED NOT NULL COMMENT '借款总金额',
  `pay_amt` decimal(10, 2) NOT NULL COMMENT '手续费',
  `create_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '插入时间',
  `update_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '信用卡资产负债表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xyk_bill_date
-- ----------------------------
DROP TABLE IF EXISTS `xyk_bill_date`;
CREATE TABLE `xyk_bill_date`  (
  `seq_no` int(0) NOT NULL AUTO_INCREMENT COMMENT '序号\r\n',
  `card_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信用卡开户行',
  `bill_date` date NOT NULL COMMENT '账单开始日',
  `pay_date` date NOT NULL COMMENT '还款日',
  `create_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '插入时间',
  `update_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `period` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账单期限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '信用卡账单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xyk_bill_date_hist
-- ----------------------------
DROP TABLE IF EXISTS `xyk_bill_date_hist`;
CREATE TABLE `xyk_bill_date_hist`  (
  `seq_no` int(0) NOT NULL AUTO_INCREMENT COMMENT '序号\r\n',
  `card_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信用卡开户行',
  `bill_date` date NOT NULL COMMENT '账单日',
  `pay_date` date NOT NULL COMMENT '还款日',
  `create_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '插入时间',
  `update_date` datetime(0) NULL DEFAULT NULL,
  `period` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账单期限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '信用卡账单历史表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xyk_info
-- ----------------------------
DROP TABLE IF EXISTS `xyk_info`;
CREATE TABLE `xyk_info`  (
  `seq_no` int(0) NOT NULL AUTO_INCREMENT COMMENT '序号\r\n',
  `card_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信用卡开户行',
  `bill_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账单日',
  `pay_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '还款日',
  `create_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '插入时间',
  `update_date` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `period` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账单期限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xyk_tran_hist
-- ----------------------------
DROP TABLE IF EXISTS `xyk_tran_hist`;
CREATE TABLE `xyk_tran_hist`  (
  `seq_no` int(0) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `card_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信用卡开户行',
  `prev_amt` decimal(17, 2) NOT NULL COMMENT '刷卡金额',
  `in_amt` decimal(17, 2) NOT NULL COMMENT '到账金额',
  `fee_amt` decimal(17, 2) NOT NULL COMMENT '刷卡手续费',
  `fee_rate` decimal(10, 4) NOT NULL COMMENT '刷卡利率',
  `use_rate` decimal(10, 4) NULL DEFAULT NULL COMMENT '资金年化利率',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `pay_type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '刷卡方式：微信，刷卡',
  `status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '0:失效；1生效',
  `create_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '插入时间',
  `update_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`seq_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
